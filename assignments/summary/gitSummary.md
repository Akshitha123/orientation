**What is Git**:
_______________________________________________________________________________________________________________________________________________________

It is a software used for version control.It is useful when a team is working on a project.It helps them to build a centralised repository so that everyone can upload code and make task simplier.

![image](/uploads/593e988584ecf11dc62fe539cc3cb8f6/image)

Git didn't come with computer. We need to install it and available on different operating systems like Windows,Linux etc..

**Installation of Git**
_______________________________________________________________________________________________________________________________________________________
It is a simple process.

**For Linux**:Open terminal and type ___sudo apt-get install Git_

**For windows**:Download the git installer and run it.

Before starting we should know some basic terminologies or commands that we use

**Git Terminology**
______________________________________________________________________________________________________________________________________________________

_Repository_: It contains folders and it supports any type of files(images,docs,HTML)

_Branch_: A Repository has a master branch by default.New branches are used for bug fixers.If you make any changes to master branch while working on new branch,updates can be pulled in.

_Commits_: Saving your work.

_Merge_: When the code is free of bugs,you can use merge that branch to master branch.

_Clone_: It is a copy of repository to local computer.

**Git Workflow:**
______________________________________________________________________________________________________________________________________________________
1. Create a branch from the repository
2. Use operations like(Create,move,delete) files.
3. Open a pull request Discuss and review your code.
4. Deploy.
5. Merge

![git_image](/uploads/f0238f90cd27b3135584d4bfef92c7f5/git_image.png)

	
	
	
