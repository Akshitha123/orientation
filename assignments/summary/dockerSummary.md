## **What is docker ?**

Docker is a tool designed to make it easier to create,deploy and run applications by using containers.It is open source platform in which everyone can contribute to meet the need.

In order to getting started with Docker we need to know some terminologies that we use

**Docker terminologies:**
_______________________________________________________________________

_Docker Image:_
It is a read only template that contains a set of instructions for creating a container that can run on the Docker platform.

_Container:_
A docker container is a running docker image.

_Docker Hub:_
It is like github for docker images and containers.
In order to work with docker we need to install it (It is supported in various Operating systems)

**Docker Installation:**
_______________________________________________________________________
###### **Installation process for Ubuntu 16.04 LTS**

     
       $ sudo apt-get update
       $ sudo apt-get install \
           apt-transport-https \
           ca-certificates \
           curl \
           gnupg-agent \
           software-properties-common
       $ curl -fsSL
    https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
        
       $ sudo apt-key fingerprint 0EBFCD88
       $ sudo add-apt-repository \
           "deb [arch=amd64] 
    https://download.docker.com/linux/ubuntu \
            $(lsb_release -cs) \
            stable nightly test"
       $ sudo apt-get update
       $ sudo apt-get install docker-ce docker-ce-cli containerd.io

      // Check if docker is successfully installed in your system
       $ sudo docker run hello-world


**Basic commands of docker:**
_______________________________________________________________________


| Command | Syntax | Operation |
| ------ | ------ | ------|
| docker ps | $ docker ps| view all containersrunning on host|
| docker start | $ docker start < container id or name>| It starts any stopped container |
| docker stop | $ docker stop < container id or name> | Stops any running container |
|docker run | $ docker run < container id or name > | creates container from docker images |
| docker rm | $ docker rm < container id or name >| deletes the containers |

**Working of docker:**
_______________________________________________________________________

##### ->Block diagram:
![docker](/uploads/fcd1fab297051edc38b500d773ad4a43/docker.png)
